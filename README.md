Documentation
------------

# Summary

A project to get the youtube channel information of users

# Infograph Youtube API integration

- Using Google + API and YouTube API, find the channels and uploaded videos of companies' (persons') accounts.

# Prerequisites

[Java](http://java.com/en/download/index.jsp) - Java 7 or later
- Apache Maven (http://maven.apache.org) 3.1 or later
[Apache Maven](http://maven.apache.org/download.cgi) - maven 3.1 or later 

- Add your G+ API Key to the file named APIKey.txt and youtube client secret at main/resources/client_secrets.json
  
  Register a developer API key from the API console at:
  
  https://code.google.com/apis/console

# Build

To build this code sample from the command line, type:

  mvn compile

To run the code sample from the command line, enter the following:

  mvn exec:java

# Database Information

Channels information saved in YouTubeChannel node

Uploaded videos by the channels saved in Youtube_videos node

Relationship: YouTubeChannel -> Upload_video -> Youtube_videos 
