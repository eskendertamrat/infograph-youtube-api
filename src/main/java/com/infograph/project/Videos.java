package com.infograph.project;

import com.google.api.client.util.DateTime;
import com.google.common.primitives.UnsignedLong;

public class Videos {

	String id;
	UnsignedLong viewCount;
	String title;
	String channelId;
	DateTime publishedAt;
}
