/*
 *  
 * Copyright 2014 Infograph. All rights reserved.
 * 
 * Version 1.0
 * 
 * @author iCog-labs
 */

package com.infograph.project;

import java.util.List;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;

public class Main {

	
	/** Global instance of the HTTP transport. */
	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	/** Global instance of the JSON factory. */
	private static final JsonFactory JSON_FACTORY = new JacksonFactory();		
	
	public static void welcome () {
		
		System.out.println("\n\n\t\t\t\t***********************************************");
		System.out.println("\t\t\t\t******* Neo4J YouTube API Integration *********");
		System.out.println("\t\t\t\t***********************************************");		
	}
	/**
	 *
	 * @param args command line args.
	 * @throws Exception 
	 */
	
	public static void main(String[] args) throws Exception {
		
			welcome();
			Neo4J neo4j = new Neo4J();
			GoogleAPI googleAPI = new GoogleAPI();
			YouTubeAPI youtubeAPI = new YouTubeAPI();
			List<Pair> item = neo4j.readFromNeo4J();
			item = googleAPI.getGoogleAccount(item, HTTP_TRANSPORT, JSON_FACTORY);
			youtubeAPI.youtubeSearch(item, HTTP_TRANSPORT, JSON_FACTORY);
			neo4j.writeToNeo4j(item);
	}
}