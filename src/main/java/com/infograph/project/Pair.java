/*
 *  
 * Copyright 2014 Infograph. All rights reserved.
 * 
 * Version 1.0
 * 
 * @author iCog-labs
 */

package com.infograph.project;

import java.util.List;

public class Pair {
	
	String name;
	String location;
	String googleUserID;
	String googleUsername;
	String youtubeChannelID;
	String videoUploadURL;	
	String type;
	List<PlaylistForm> videosByChannelId;
	List<Videos> videosSearch;
}
