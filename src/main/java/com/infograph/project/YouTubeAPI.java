/*
 *  
 * Copyright 2014 Infograph. All rights reserved.
 * 
 * For querying YouTube API
 * 
 * Version 1.0
 * 
 * @author iCog-labs
 */

package com.infograph.project;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.java6.auth.oauth2.FileCredentialStore;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.Channel;
import com.google.api.services.youtube.model.ChannelListResponse;
import com.google.api.services.youtube.model.PlaylistItem;
import com.google.api.services.youtube.model.PlaylistItemListResponse;
import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoListResponse;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

public class YouTubeAPI {
    
	private static YouTube youtube;
	
	/**
	* Authorizes the installed application to access user's protected data.
	*
	* @param scopes list of scopes needed to run upload.
	* @param HttpTransport
	* @param JsonFactory
	* @return Credential
	*
	*/
	private static Credential authorize(List<String> scopes, HttpTransport HTTP_TRANSPORT, JsonFactory JSON_FACTORY) throws Exception {

		// Load client secrets
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(
				JSON_FACTORY, YouTubeAPI.class.getResourceAsStream("/client_secrets.json"));
	  	// Checks that the defaults have been replaced (Default = "Enter X here").
	  	if (clientSecrets.getDetails().getClientId().startsWith("Enter")
	  			|| clientSecrets.getDetails().getClientSecret().startsWith("Enter ")) {
	  		System.out.println(
	  				"Enter Client ID and Secret from https://code.google.com/apis/console/?api=youtube"
	  				+ "into youtube-cmdline-myuploads-sample/src/main/resources/client_secrets.json");
	  		System.exit(1);
	  	}
	  	// Set up file credential store.
	  	FileCredentialStore credentialStore = new FileCredentialStore(
	  			new File(System.getProperty("user.home"), ".credentials/youtube-api-myuploads.json"),
	  			JSON_FACTORY);
	  	// Set up authorization code flow.
	  	GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
	  			HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, scopes).setCredentialStore(credentialStore)
	  			.build();
	  	// Build the local server and bind it to port 8000
	  	LocalServerReceiver localReceiver = new LocalServerReceiver.Builder().setPort(8080).build();
	  	// Authorize.
	  	return new AuthorizationCodeInstalledApp(flow, localReceiver).authorize("user");
	}
	  
	/**
	* 
	* Get the Youtube channel Id for the Google + account
    *
    * @param iteratorSearchResults 
    * @param google+ ID
    * @param query
    * @param youtube instance
    * @return Channel ID
    */
	private static String getChannelId(Iterator<SearchResult> iteratorSearchResults, 
			String googleId, String query, YouTube youtube) throws IOException {

		String youTubeChannelId = null;
		while (iteratorSearchResults.hasNext()) {
			SearchResult singleVideo = iteratorSearchResults.next();
			ResourceId rId = singleVideo.getId();
			if (rId.getKind().equals("youtube#channel")) {			
				YouTube.Channels.List channels = 
						youtube.channels().list("id,contentDetails").setMaxResults(1L).
						setPart("contentDetails").setId(rId.getChannelId());
				ChannelListResponse channelResult = channels.execute();
				List<Channel> channelsList = channelResult.getItems();
				if (channelsList.isEmpty()) {
					System.exit(0);
				}					
				channelsList.get(0).getContentDetails().getRelatedPlaylists().getUploads();
				Map<String, Object> a = channelsList.get(0).getContentDetails().getUnknownKeys();
				if(channelsList.get(0).getContentDetails().getUnknownKeys().containsKey("googlePlusUserId")) {
					for(String xk : a.keySet()) {
						if(googleId.equalsIgnoreCase((String) a.get(xk))) {
							youTubeChannelId = channelsList.get(0).getId();
						}
					}
				}
			}
		}
		return youTubeChannelId;
	}
		
	/**
	 *
	 * Crawling videos whose title or description match person/company entities
	 * 
	 * @param Item 
	 * @throws IOException 
	 * 
	 */
	private static void videosMatch(Pair item) throws IOException {

		String queryTerm = item.name;
		//specified 50 as maximum because the default is 5. We are going to take the top ten based on view count.
		YouTube.Search.List search = youtube.search().list("id,snippet").
				setQ(queryTerm).setMaxResults(50L).setType("video");		
		SearchListResponse searchResponse = search.execute();
        List<SearchResult> searchResultList = searchResponse.getItems();
        List<String> videoIds = new ArrayList<String>();
        List<Videos> topVideos = new ArrayList<Videos>();  
        if (searchResultList != null) {
            // Merge video IDs
            for (SearchResult searchResult : searchResultList) {
                videoIds.add(searchResult.getId().getVideoId());
            }
            Joiner stringJoiner = Joiner.on(',');
            String videoId = stringJoiner.join(videoIds);
            // Call the YouTube Data API's youtube.videos.list method to
            // retrieve the resources that represent the specified videos.
            YouTube.Videos.List listVideosRequest = youtube.videos().list("snippet", "statistics").setPart("snippet,statistics,contentDetails").setId(videoId);
            listVideosRequest.getUriTemplate();
            VideoListResponse listResponse = listVideosRequest.execute();
            List<Video> videoList = listResponse.getItems();            
            if (videoList != null) {
                List<Videos> videos = crawledVideos(videoList.iterator());
                //Sorting the top ten videos by most views
                Collections.sort(videos, new Comparator<Videos>() {
                	@Override
                	public int compare(Videos firstVid, Videos secondVid) {
            
                		return  secondVid.viewCount.compareTo(firstVid.viewCount);
                	}
                });                                     		
        		int count = 0;
                for (Videos v: videos) {
                	if(count<10) {
                		count++;
                		topVideos.add(v);
                	}
                }               
            }
            item.videosSearch = topVideos;
        }
	}	

    /** 
     * 
     * get the metadata records on the videos
     *
     * @param iteratorVideoResults Iterator of Videos to print
     *
     * @return the metadata about found videos 
     */
    private static List<Videos> crawledVideos(Iterator<Video> iteratorVideoResults) {

        List<Videos> videos = new ArrayList<Videos>();
        while (iteratorVideoResults.hasNext()) {
        	
            Video singleVideo = iteratorVideoResults.next();
            Videos video = new Videos();
            video.id = singleVideo.getId();
            video.channelId = singleVideo.getSnippet().getChannelId();
            video.title = singleVideo.getSnippet().getTitle();
            video.publishedAt = singleVideo.getSnippet().getPublishedAt();
            video.viewCount = singleVideo.getStatistics().getViewCount();
            videos.add(video);
        }
        return videos;
    }

	
	
	/**
	* 
	* Get the uploaded videos by the channel
	*
	* @param size
	* @param iterator
	* @param Pair of Company/Person values 
	*
	* @param iterator of Playlist Items from uploaded Playlist
	*/
	private static void youtubeVideos(int size, Iterator<PlaylistItem> playlistEntries, Pair item) {
	        
		int i = 0;
	    List<PlaylistForm> playlists = new ArrayList<PlaylistForm>();
	    while (playlistEntries.hasNext()) {
	    	PlaylistItem playlistItem = playlistEntries.next();
	        PlaylistForm playlistForm = new PlaylistForm();
		    playlistForm.id = playlistItem.getContentDetails().getVideoId();
		    playlistForm.name = playlistItem.getSnippet().getTitle();
		    playlists.add(i, playlistForm);	            
	    }
	    item.videosByChannelId = playlists;
	}
	
    /**
	* Initializes YouTube object to search for Channels/videos on YouTube
	*
	* @param companies/persons whose YouTube account are going to be searched
	* @param HttpTransport
	* @param JsonFactory 
	* 
	* @throws Exception 
	*
	*/
	public void youtubeSearch(List<Pair> item, HttpTransport HTTP_TRANSPORT, JsonFactory JSON_FACTORY) throws Exception {
		
		System.out.println("\n\tQuerying the Youtube API for accounts of " + item.size() + " records");		
	    List<String> scopes = Lists.newArrayList("https://www.googleapis.com/auth/youtube");
	    Credential credential = authorize(scopes, HTTP_TRANSPORT, JSON_FACTORY);
	    youtube = new YouTube.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(
	            "infograph-youtube-api").build();
	    int count = 0;
		for (Pair pair: item) {	
			String queryTerm = pair.googleUsername; 
	    	YouTube.Search.List search = youtube.search().list("id,snippet").setQ(queryTerm).setType("channel");
	    	SearchListResponse searchResponse = search.execute();
	    	List<SearchResult> channelList = searchResponse.getItems();
	    	if ((channelList != null) & (pair.googleUserID !=null)) {
	    		pair.youtubeChannelID = getChannelId(channelList.iterator(), pair.googleUserID, queryTerm, youtube);
	    		if(pair.youtubeChannelID != null) {
	    			count++;
	    			YouTube.Channels.List searchedChannels = youtube.channels().list("contentDetails").setId(pair.youtubeChannelID);
	    			ChannelListResponse channelResponse = searchedChannels.execute();
	    			List<Channel> channels = channelResponse.getItems();
	    			pair.videoUploadURL = 
			    			channels.get(0).getContentDetails().getRelatedPlaylists().getUploads();
	    			YouTube.PlaylistItems.List playlistItemRequest =
	                        youtube.playlistItems().list("id,contentDetails,snippet").
	                        setPlaylistId(pair.videoUploadURL).setFields
	                        ("items(contentDetails/videoId,snippet/title,snippet/publishedAt),nextPageToken,pageInfo");		    			
	    			String nextToken = "";
	    			List<PlaylistItem> playlistItemList = new ArrayList<PlaylistItem>();
	    			do {
	                    playlistItemRequest.setPageToken(nextToken);
	                    PlaylistItemListResponse playlistItemResult = playlistItemRequest.execute();
	                    playlistItemList.addAll(playlistItemResult.getItems());
	                    nextToken = playlistItemResult.getNextPageToken();
	                } while (nextToken != null);
	                youtubeVideos(playlistItemList.size(), playlistItemList.iterator(), pair);
	    		}
	    	}	
    		videosMatch(pair);
	    }
		System.out.println("\n\tYoutube Channel found for " + count + " records");
	}
}
