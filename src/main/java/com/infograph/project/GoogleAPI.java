/*
 *  
 * Copyright 2014 Infograph. All rights reserved.
 * 
 * For handling the reading from/writing into Neo4J database
 * 
 * Version 1.0
 * 
 * @author iCog-labs
 */

package com.infograph.project;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.util.List;

import com.google.api.client.http.HttpTransport;

import com.google.api.client.json.JsonFactory;

import com.google.api.services.plus.Plus;
import com.google.api.services.plus.PlusRequestInitializer;
import com.google.api.services.plus.model.PeopleFeed;
import com.google.api.services.plus.model.Person;

public class GoogleAPI {
	
	/**
	 * Search the GoogleAPI for account
	 * @param item
	 * @param HTTP_TRANSPORT
	 * @param JSON_FACTORY
	 * @return G+ ID and Username
	 * @throws IOException
	 */
	public List<Pair> getGoogleAccount(List<Pair> item, HttpTransport HTTP_TRANSPORT, JsonFactory JSON_FACTORY) throws IOException {
		
		BufferedReader br = new BufferedReader(new FileReader("APIKey.txt"));
		String API_KEY = br.readLine();		
		br.close();
		int count = 0;
		System.out.println("\n\tQuerying the Google+ API for accounts of " + item.size() + " records");
		Plus plus = new Plus.Builder(HTTP_TRANSPORT, JSON_FACTORY, null).setApplicationName("infograph").
				setGoogleClientRequestInitializer(new PlusRequestInitializer(API_KEY)).build();		
		for (Pair pair : item) {
			String personLocation = pair.name + " " + pair.location;
			Plus.People.Search searchPeople = plus.people().search(personLocation).setMaxResults(1L);
			PeopleFeed peopleFeed = searchPeople.execute();
			List<Person> people = peopleFeed.getItems();
			if(people.isEmpty()) {
				searchPeople = plus.people().search(pair.name);
				peopleFeed = searchPeople.setMaxResults(1L).execute();
				people = peopleFeed.getItems();			
			}
			for (Person person : people) {
				if (!(person.getDisplayName().toLowerCase().contains(pair.name.toLowerCase()))) {
					searchPeople = plus.people().search(pair.name);
					peopleFeed = searchPeople.setMaxResults(1L).execute();
					people = peopleFeed.getItems();
				}
			}
			int pageNumber = 1;
			while (people != null && pageNumber <= 1) {				
				pageNumber++;		  
				for (Person person : people) {
					if ((person.getDisplayName().toLowerCase().contains(pair.name.toLowerCase()))) {
						pair.googleUsername = person.getDisplayName();
						pair.googleUserID = person.getId();
						count++;		
					}
				}
			}
		}
		System.out.println("\n\tGoogle+ account found for " + count + " records");		
		return item;
	}
}
