/*
 *  
 * Copyright 2014 Infograph. All rights reserved.
 * 
 * For handling the reading from/writing into Neo4J database
 * 
 * Version 1.0
 * 
 * @author iCog-labs
 */

package com.infograph.project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class Neo4J {
	
	final String dbpath = "http://localhost:7474/db/data/";
	ExecutionResult result;
	GraphDatabaseService graphDb = new GraphDatabaseFactory().newEmbeddedDatabase( dbpath );
	ExecutionEngine engine = new ExecutionEngine( graphDb );
	Client client = Client.create();
	WebResource cypher = client.resource( dbpath + "cypher"); 
	String response = null;
	ClientResponse clientResponse = null;
	
	/**
	 * Read pair of name-location from Neo4j database
	 * @param query
	 * @return name-location
	 */
	
	public List<Pair> readFromNeo4J () {
				
		String query = null;
		List<String> data = Arrays.asList("Company", "Person");		
		List<Pair> pairs = new ArrayList<Pair>();  		
		for (String s: data) {
			System.out.println("\n\tStart Reading name-location pairs of " + s + " from Neo4J database ...");
			query = "{\"query\":\"MATCH (" + s +")-[:LOCATED_IN]->(Location)" +
					"RETURN " + s +".name, Location.name\"}";
			response = cypher.accept(
					MediaType.APPLICATION_JSON_TYPE).
					entity(query, MediaType.APPLICATION_JSON_TYPE).
					post(String.class);
			JSONObject obj = (JSONObject) JSONValue.parse(response);	    
			JSONArray jsonData = (JSONArray)obj.get("data");
			for ( int i = 0; i < jsonData.toArray().length; i++ ) {	    	
				JSONArray dataPair = (JSONArray)jsonData.get(i);
				Pair pair = new Pair();
				pair.name = (String) dataPair.get(0);
				pair.location = (String) dataPair.get(1);
				pair.type = s;
				pairs.add(pair);
			}
			System.out.println("\n\tFinished Reading name-location pairs of " + s + " from Neo4J database\n\n");
		}
		return pairs;
	}
	
	/**
	 * 
	 * Write to the pair of values to the Neo4J database
	 * @param item - Pair of values to write to the database
	 *
	 */
	
	public void writeToNeo4j (List<Pair> item) {
		
		String headerName = "https://www.youtube.com/channel/";
		String videoHeader = "https://www.youtube.com/watch?v=";
		System.out.println("\n\n\t*********************************************************************");
		System.out.println("\tStart writing Youtube channels and uploaded videos to Neo4J database ...");		
		int channels = 0, videos =0, metadata = 0;
		for (Pair p : item) {			
			if (p.youtubeChannelID != null) {
				String youtubeQuery = "{\"query\":\"MERGE (:YouTubeChannel{name:\'"+ p.name + 
						"\',location:\'" + p.location + "\',type:\'" + p.type + "\',youtube_url: \'" + 
						headerName + p.youtubeChannelID +  "\'})\"}";				
				channels++;
				if (p.videosByChannelId != null) {
					clientResponse = cypher.accept( MediaType.APPLICATION_JSON_TYPE )
					        .entity( youtubeQuery, MediaType.APPLICATION_JSON_TYPE )
					        .post(ClientResponse.class);
					for(PlaylistForm playlist : p.videosByChannelId) {
						String videoQuery = "{\"query\":\"MERGE (:Youtube_videos{youtube_url:\'"+ headerName + 
								p.youtubeChannelID + "\',video_name:\'" + playlist.name + "\',video_url:\'" + 
								videoHeader + playlist.id + "&list=" + p.videoUploadURL + "\'})\"}";				
						videos ++;
						clientResponse = cypher.accept( MediaType.APPLICATION_JSON_TYPE )
						        .entity( videoQuery, MediaType.APPLICATION_JSON_TYPE )
						        .post(ClientResponse.class);
					}
				}			
				String mergeQuery = "{\"query\":\"MATCH (a:YouTubeChannel{youtube_url:\'" + headerName + 
						p.youtubeChannelID + "\'}),(b:Youtube_videos{youtube_url:\'" + headerName + p.youtubeChannelID + "\'})" +
						" MERGE (a)-[r:Upload_video]->(b)" +
						" RETURN r\"}";
				clientResponse = cypher.accept( MediaType.APPLICATION_JSON_TYPE )
			        	.entity( mergeQuery, MediaType.APPLICATION_JSON_TYPE )
			        	.post(ClientResponse.class);
			}
			for (Videos v : p.videosSearch) {
				String videosMetadata = "{\"query\":\"MERGE (:Youtube_videos_metadata{name:\'"+ p.name + 
						"\',video_id:\'" + v.id + "\',view_count:\'" + v.viewCount + "\',title:\'" + 
						v.title + "\',channel_id:\'" + v.channelId + "\',published_at:\'" + v.publishedAt + "\'})\"}";
				clientResponse = cypher.accept( MediaType.APPLICATION_JSON_TYPE )
			        	.entity( videosMetadata, MediaType.APPLICATION_JSON_TYPE )
			        	.post(ClientResponse.class);
				metadata++;
			}
			
		}
		System.out.println("\t" + channels + " YouTube Channels added to the Neo4J database");
		System.out.println("\t" + videos + " YouTube Videos registered by channel ID added to the Neo4J database");					
		System.out.println("\t" + metadata + " YouTube Videos crawled about the companies/persons added to the Neo4J database");					
		System.out.println("\t*********************************************************************");
	}
}